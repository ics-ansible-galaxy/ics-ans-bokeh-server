# ics-ans-bokeh-server

Ansible playbook to install a Bokeh Server.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## License

BSD 2-clause
